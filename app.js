'use strict'
/**
*
*  This is the module where the hapi server is started.
*  Routes with swagger doc are defined in this file as well.
*
*
**/
module.exports = function () {
  let hapi = require('hapi'),
    good = require('good'),
    winston = require('winston'),
    RegisterPublicRoutes = require('./lib/routes/register-public-routes'),
    RegisterPrivateRoutes = require('./lib/routes/register-private-routes'),
    registerPublicRoutes,
    registerPrivateRoutes,
    nconf = require('nconf'),
    config,
    overrides = {},
    path = require('path'),
    dependencies = {},
    pack = require('./package'),
    repoInfo = require('./repo-info'),
    publicRoutePrefix = '/v1/blueprint',
    privateRoutePrefix = '/v1/_blueprint',
    swaggeredOptions = {
      info: {
        'title': pack.description,
        'version': pack.version
      },
      endpoint: `${publicRoutePrefix}/swagger.json`,
      'tagging': {
        'mode': 'tags'
      }
    },
    server,
    portConfig,
    winstonLogger,
    ConfigAccessor = require('ecom-base-lib').ConfigAccessor,
    EComGoodWinston = require('ecom-base-lib').EComGoodWinston,
    EComInternalCredFilter = require('ecom-fabric-lib').Hapi.EComInternalCredFilter,
    EComTools = require('ecom-tools-lib').RegisterAllRoutes,
    _ = require('lodash'),
    RequestID = require('ecom-fabric-lib').Hapi.RequestID,
    WinstonFilters = require('ecom-base-lib').WinstonFilters,
    repoConfig={};

  nconf.argv().env();
  if (nconf.get('overrideConfig')) {
    overrides = require(nconf.get('overrideConfig'));
    nconf.overrides(overrides);
  }
  nconf.defaults(require('./config/config'));
  config = nconf.get();
  server = new hapi.Server();
  dependencies.logger = server;

  let configAccessor = new ConfigAccessor(dependencies, config);
  configAccessor.init().then(function () {
    let result = configAccessor.configKeyMap[config.environment];
    nconf.set(pack.name, _.merge(config[pack.name], result));
    config = nconf.get();
    
    winstonLogger = new (winston.Logger)({
      exitOnError: false,
      transports: [
        new winston.transports.DailyRotateFile({
          name: 'file',
          datePattern: config.server.logging.datePattern,
          filename: path.join(config.server.logging.logsDirectory, config.server.logging.appLogName),
          level: 'debug',
          maxsize: 1024 * 1024 * 10
        })
      ]
    });

    repoInfo.updateRepoAutoConfig(repoConfig);

    repoConfig = repoConfig.config;

    winstonLogger.filters.push(WinstonFilters.sensitivePayloadFilter);

    portConfig = config.server.port;

    let publicConnection = server.connection({
      port: portConfig.http,
      routes: {
        cors: true
      },
      labels: ['public']
    });

    let privateConnection = server.connection({
      port: portConfig._http,
      routes: {
        cors: true
      },
      labels: ['private']
    });

    server.register([
      {
        register: RequestID,
        options: {}
      },
      {
        register: good,
        options: {
          reporters: [{
            reporter: require('good-console'),
            events: {
              request: '*',
              response: '*',
              error: '*'
            }
          }, new EComGoodWinston({
            request: '*',
            response: '*',
            log: '*',
            error: '*',
            ops: '*'
          }, winstonLogger)
          ]
        }
      },
      require('h2o2'),
      require('inert'),
      require('vision'),
      {
        register: EComInternalCredFilter,
        options: {}
      },
      {
        register: require('hapi-swaggered'),
        options: swaggeredOptions
      },
      {
        register: require('hapi-swaggered-ui'),
        options: {
          'path': `${publicRoutePrefix}/documentation`
        }
      },
      {
        select: ['private'],
        register: EComTools,
        routes: {
          prefix: privateRoutePrefix
        },
        options: {
          dependencies: dependencies,
          config: repoConfig
        }
      }], function (err) {
      if (err) {
        throw err; // something bad happened loading the plugin
      }
      
      registerPublicRoutes = new RegisterPublicRoutes(dependencies, config);
      registerPrivateRoutes = new RegisterPrivateRoutes(dependencies, config);

      server.auth.strategy('ecom-internal-cred', 'ecom-internal-cred', {
        dependencies: dependencies,
        config: config
      });
      server.auth.strategy('ecom-internal-cred-auth', 'ecom-internal-cred', {
        dependencies: dependencies,
        config: config,
        authenticated: true
      });
      server.auth.strategy('ecom-internal-auth-guest', 'ecom-internal-cred', {
        dependencies: dependencies,
        config: config,
        authenticated: false
      });

      try {
        registerPublicRoutes.registerRoutes(publicConnection);
        registerPrivateRoutes.registerRoutes(privateConnection);
        server.start(function () {
          _.forEach(server.connections, function (connection) {
            server.log('Server started at: ' + connection.info.uri);
          });
        });
      } catch (err) {
        server.log('error', err);
        process.exit();
      }
    });
  });
}
