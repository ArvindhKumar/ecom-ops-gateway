'use strict';

const repoInfo = require('../../repo-info'),
  Authentication = require('../modules/authentication'),
  Authorisation = require('../modules/Authorisation'),
  AppProxy = require('../modules/app-proxy'),
  EcomProxy = require('../modules/ecom-proxy');

class RegisterPublicRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.repoConfig = {};
    repoInfo.updateRepoAutoConfig(this.repoConfig);
    this.repoConfig = this.repoConfig.config;
    this.config.appId = this.repoConfig.appId = repoInfo.name;
    this.authentication = new Authentication(dependencies, config);
    this.authorisation = new Authorisation(dependencies, config);
  }


  registerRoutes(server) {
    const me = this;
    server.log('Registering public routes for Blueprint service');

    server.route({
      method: 'GET',
      path: '/app/{restOfThePath*}',
      config: {
        pre: [
          {method: me.authentication.authenticateRequest, assign: 'isAuthenticated'},
          {method: me.authorisation.authoriseRequest, assign: 'isAuthorised'}
        ],
        handler: {
          proxy: {
            mapUri: AppProxy,
            passThrough: true,
            xforward: true
          }
        },
        bind: me,
        description: 'App Route',
        tags: ['api', 'blueprint'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/api/{restOfThePath*}',
      config: {
        pre: [
          {method: me.authentication.authenticateRequest, assign: 'isAuthenticated'},
          {method: me.authorisation.authoriseRequest, assign: 'isAuthorised'}
        ],
        handler: {
          proxy: {
            mapUri: EcomProxy,
            passThrough: true,
            xforward: true
          }
        },
        bind:me,
        description: 'Api Route',
        tags: ['api', 'blueprint'],
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });
  }
}

module.exports = RegisterPublicRoutes;
