'use strict';

const repoInfo = require('../../repo-info');


class RegisterPrivateRoutes {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
    this.repoConfig = {};
    repoInfo.updateRepoAutoConfig(this.repoConfig);
    this.repoConfig = this.repoConfig.config;
    this.config.appId = this.repoConfig.appId = repoInfo.name;
  }

  registerRoutes(server) {
    server.log('No private routes to register for Blueprint service');
  }
}

module.exports = RegisterPrivateRoutes;
