"use strict";

const AuthorisationServiceClient = require('../clients/authorisation-service-client'),
  _ = require('lodash'); 

class Authorisation {

  authoriseRequest(request, reply) {
    let me = this;
    let authServiceClient = new AuthorisationServiceClient(me.dependencies, me.config, request);
    authServiceClient.authorise('tok').then(function (result) {
      let config = me.config;
      let permissionsToValidate = null;
      _.each(config["ecom-ops-gateway"].rules, function(rule){
        let match = request.path.match(rule.urlPattern);
        if(request.route.method == rule.method && match && match.length > 0){
          permissionsToValidate = rule.permissions;
          return;
        }
      })

      if(permissionsToValidate && result){
        _.forOwn(permissionsToValidate, function(value, key) {
          if(!(result[key] && result[key].indexOf(permissionsToValidate[key]) > -1)) {
            return reply("Forbidden").code(403).takeover();
          }
        });
        reply(true);
      }
      else{
        reply("Forbidden").code(403).takeover(); 
      }
      
    }, function (error) {
      reply(error);
    });
  }
}

module.exports = Authorisation;
