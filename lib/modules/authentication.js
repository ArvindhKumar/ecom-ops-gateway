"use strict";

const AuthenticationServiceClient = require('../clients/authentication-service-client');

class Authentication {
  constructor(dependencies, config) {
    this.config = config;
    this.dependencies = dependencies;
  }

  authenticateRequest(request, reply) {
    let me = this;
    let authServiceClient = new AuthenticationServiceClient(me.dependencies, me.config, request);
    authServiceClient.authenticate(request).then(function (result) {
      reply(result);
    }, function (error) {
      reply(error);
    });
  }
}

module.exports = Authentication;
