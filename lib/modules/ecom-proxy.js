"use strict";

const EcomProxy = function (request, callback) {
  let url = 'http://localhost:4500/'+request.params.restOfThePath;
  callback(null, encodeURI(url));
};

module.exports = EcomProxy;
