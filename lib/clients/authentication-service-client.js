"use strict";

const BaseServiceClient = require('ecom-base-lib').BaseServiceClient,
  co = require('co'),
  _ = require('lodash');

class AuthenticationServiceClient extends BaseServiceClient {
  constructor(dependencies, config, requestContext, isExternalCall=false, options) {
    options = _.assignIn(options, {doNotLogPayload: true});
    super(dependencies, config, requestContext, isExternalCall, options);
    this.url = 'http://localhost:4500/authenticate';
  }

  authenticate() {
    const me = this;
    return co(function*() {
      const url = me.url;
      try {
        let result = yield me._get(url, {}, null);
        return result;
      } catch (error) {
        me.error('AuthenticationServiceClient', 'authenticate', error);
        throw error;
      }
    });
  }
}

module.exports = AuthenticationServiceClient;
